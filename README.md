# a collective research: simplicity in websites

Making website by doing less and thinking more (?)

Collective research around the ideas (and trends) (and counter-mouvement) (and history) of: low-tech / raw / brutalist / hand-crafted / simple websites (a lot of words that need to be sorted out!).

<!-- Doriane:
personnaly since i've started making website ✳️ i've been feeling quite inspired by projects that explores those ideas of simplicity: sometimes encountering nice websites (made this year or in the 90's), but also more englobing projects arround web technologies, sometimes poetry sometimes in-depth technical tutorials; and at the same time the trends around those ideas of simplicity in website makes it feels like a trap that needs to be unfolded. 
-->

It can be in terms of... (of course those are linked, but some websites can focus more on some points and less on others).

* **visual**
  * defaultism: embracing or dealing with web defaults aesthetics
  * constrainism: designs that works with limited or constrained definitions or resources
* **web tech**
  * static over dynamic
  * javascript-less
  * taking advantages of already-there techs (instead of re-creating them with external libs)
    * achors/permalinks
    * specific CSS selectors
    * browser functionnalities
    * emails
    * RSS
  * size (small sized websites)
    * dealing with images
    * dealing with fonts (default `font-familly`)
    * omitings tags
