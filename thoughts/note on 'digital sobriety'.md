author: Doriane
date: 22/04/16

## Note on "digital sobriety"

I don't like the term _"digital sobriety"_ (or _"sobriété numérique"_), as when using it we quickly go into a dense fog on what we are actually talking about (see the points listed in the README).
For example some website have super raw visual design, but can be in fact dynamic database-driven website, or for example you can use wordpress on a blog with minimal / no-css, or you could have the opposite: super low-tech website, but using explosive pop colors and effect. And this is not the only direction on which a website can pretend to _sobriety_, it can be because it had to be made quickly under restrictive constraint, though using not-very-low-tech systems for templates and generation, and ending up with something that has a very standard raw feels. Or even a one page low-tech website hosted on the least ecological services? Is wikipedia an example of _"digital sobriety"_ because it feels early internet? (I have no idea). It can quickly feels like a trend more than anything.

However i personnaly love _simplicity_ (when done right). the simpler the idea the stronger it hits me.
If you would say that _simplicity_ is: 

> _"not doing more than what is right"_

then, even though it stays as vague as the other terminology in terms of what specificities are we talking about, at least it shift the focus, the aim: it's not about _"pretending"_ or _"looking"_ anymore (looking minimal, looking efficient, looking low-tech, etc), but about finding out **what feels right for what you are doing** and focus on that as much as you can.
In way it forces us more to clarify about what we're speaking when we say _simple_ (I like the simplicity of I think this part should have been a more simple), while _"digital sobriety"_ try to act like it's clearly defined and speaks about the thing as a whole, blurring what is actually happening.