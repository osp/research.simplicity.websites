# References

A list of texts, any other mediums, that speak about the criteria listed in the README.md.
For example:
* why does it matter
* tutorials
* technical in-depth explanaitions
* what are the consequences of making such websites
  
Please don't just put a link, but write a little sentence about what is the reference about specifically.

## List

### http://html.energy/

> HTML energy is all around us and in this very website.

I love how this project focuses on the energy of HTML, instead of trying to say anything technical it poses the question: how do we feel HTML. We interact daily with HTML, it can be nostaligic, wild, akward, fun, liberating, corporate, etc; but it has an energy. It position over complexity on website has something that goes against the _grain_ or _materiality_ of this HTML energy.

> Anyone who wants to publish on the web can write HTML.
> This accessibility and ease of use is where its energy resides.

The project also goes on the act of writing HTML, as a practice (something that can be like sport, meditation, writing on a planner).

They made _personnal_ podcasts about writing HTML, with cool designers :-)

#practice, #social, #accessibility


### http://contemporary-home-computing.org/prof-dr-style/

[to write]

\+ links to the 2 other vernacular texts

#defaultism, #90, #vernacular


### Why CSS is weird https://www.youtube.com/watch?v=aHUtMbJw8iA&ab_channel=MozillaDeveloper

The paradox of CSS was for document and is now for apps.

What does it mean to "design" in a medium where the end point can not be predicted and will change: tech support, screen format/size, interface (keyboard, touch, headphone - listening to website...), etc. Designing for tech of the past & the future at the same time.

> There is a problem with design on the web. It's problematic to design in a way that is going to be device agnostic

Tim Berneer lee stating at the start of internet (http://info.cern.ch/hypertext/WWW/) how HTML was designed with simplicity in mind for this platform agnostic idea

> It is required that HTML be a common language between all platforms. This implies no device-specific markup, or anything which requires control over fonts or colors, for example. 
> 
> Tim Berneer lee

The idea of giving up controls on your design.
> _audience_ now controls the display of my _content_, which wasn't true for the printing press.

> Design not longuer control but suggest

How the declarativeness of CSS makes sense has a way to deal with this problematic.
Things like "I want that bigger than that", "Align that with that if you can", "Show this in red".
Describing the intent, not the result.
declarativeness allows for a fluidity of interpretations on the screen that is dependant on context (screen-size, support, device, etc).
This is why prototyping a website in figma or inkscape doesn't make much sence to me, as this break the poetic approach of building a website with language and goes against this declarativeness of HTML/CSS.

> It has to break gracefully (a form of resilience)

by saying

> CSS is a presentation layer we're ok loosing

It focus back on everything else that make a website than it's presentation layer

Metaphor of writing a piece for a theater, that can be performed differently depending on the actors, stage, etc.
It's **contextual**.

Reverse this lack of control, has having much fixed control is in fact a limitation on what i can be.

> Control is a limitation of printed page.
> 
> John Allsopp

#CSS, #device-agnostic, #defaultism, #declarativeness


### HTML IS a Programming Language (Imperative vs Declarative) https://www.youtube.com/watch?v=4A2mWqLUpzw&ab_channel=Computerphile

#HTML, #declarativeness


### http://gmpg.org/xfn/intro

> The web is more a social creation than a technical one.

this but with this added lecture: then every layer of technology we had is an added layer between the user social inter-action, between the user and the web. for every layer added ask: is it adding something in the term of the social (living and organic) experience that is the internet, outside of the pure technicality of it.

#social


### https://gossipsweb.net/

> Gossip’s Web the directory of handmade webpages

what i like is that this operate on multiple more theorical level: it's a search engine for those kind of website by creating a collectivity, it focuses on the fact their handmade (by opposition to industrial ?), it countains a lot of _personnal websites_, showing how thoses tends to appear naturaly - because that what we feel is right/nice to do with the web :-) (by opposition to organisation, brands, etc).

#handcraft, #social


### https://asktog.com/atc/the-third-user/

the apple design phylosophy is made for the buyer user type; removing the embarassing tools and branding minimalism a paradigm that make the design more sellable, but less usable.

Safari deciding to hide the scroll bars, hide the address bar, etc. the idea that for a lot of user controls are confusing and embarrassing, but to go behind this confusion is to become a proper user and not a buyer.

it's a interesting example of presenting the "minimalism purity" - that some people (inspired by apple) confuse with clarity - as a selling strategy (the ferrari - design to disguise what is inside, but sell you an idea), where showing all the "default confusing buttons" is actually the empowering default minimalism (the tractor - design to show what is inside, selling you what you can use).

#buyer, #user, #design


### https://shkspr.mobi/blog/2021/01/the-unreasonable-effectiveness-of-simple-html/

the unreasonable effectiveness of simple html = website that can open on crappy browser such as: PSP, smart TV, car browser (?).

this recenter what is essential: for example accessibility feature such as alt text, can be more important than your CSS or JS based design. 

#accessibility, #raw


### http://tombubul.info/tenets.html

[to write]


### https://sustainable.websites.ngo/

a list of references on sustainables website

#sustainability, #lowtech


### Omitings HTML tags

This small blogpost on writing plain HTML:
https://utcc.utoronto.ca/~cks/space/blog/web/PlainHTMLAppeal

With this discussion on Hackernews, also about the cloudiness of some static site generator:
https://news.ycombinator.com/item?id=31217434

Made me discover this:
https://google.github.io/styleguide/htmlcssguide.html#Optional_Tags

Computerphile video on this
https://www.youtube.com/watch?v=-csXdj4WVwA&ab_channel=Computerphile


### https://utcc.utoronto.ca/~cks/space/blog/web/PlainHTMLAppeal

This small blogpost on writing plain HTML.


<!-- ### my text on artisanat and website (handshaped) -->

