# Inspirations

A list of examples that inspires us.
They should at least follow two or more criteria defined in the README.md.

Please don't just put a link, but write a little sentence about why this examples is inspiring specifically.

## List

### https://solar.lowtechmagazine.com/

**solar-powered** (thinking about internet materiality and ecology), **going offline** (embracing the constraint of energy), small sized, dealing with images, default fonts, defaultism, already-there tech (RSS, printing/archiving the website by releasing a book of it).


### http://meyboom.osp.kitchen/

small sized, default fonts, defaultism, javascript-less, **an svg as navigation (a map)** with anchor links (interactivity with constraints).

### https://ci22.href.blue/

**select tag as navigation** (interactivity with constraints), default fonts, making a simple yet effective design with the transform css property (constrainism).

### https://eric.young.li/

**defaultism**, (nearly) **CSS-less**, default fonts, use of less-known html tags for design purposes, use of the target selector for navigation, details tags for javascript-less interactivity.

### https://john-doe.neocities.org

one-file website, **small-sized (less than 10KB)**, **use of the target selector to emulate multi-pages** (javascript-less interactivity), inline-footnote with labels and checkboxes.

<!-- https://wiki.xxiivv.com/site/soies.html -->
<!-- https://veryinteractive.net -->